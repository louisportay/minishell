# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lportay <lportay@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/17 21:45:56 by lportay           #+#    #+#              #
#    Updated: 2017/09/26 17:08:36 by lportay          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: clean fclean re
vpath %.c srcs/
export $(DEBUG)

CC= gcc-7
DEBUG=yes
OPT=LIB

ifeq ($(DEBUG), yes)
	CFLAGS= -Wall -Wextra -Werror -g -I $(INCLUDE) -I $(LIBDIR)$(INCLUDE)
else ifeq ($(DEBUG), sanitize)
	CFLAGS= -Wall -Wextra -Werror -g -fsanitize=address -I $(INCLUDE) -I $(LIBDIR)$(INCLUDE)
else
	CFLAGS= -Wall -Wextra -Werror -I $(INCLUDE) -I $(LIBDIR)$(INCLUDE)
endif

INCLUDE= includes/
vpath %.h $(INCLUDE)
HEADERS= minishell.h
SRCS= main.c\
      minishell.c\
      builtin.c\
      tools.c\
      search.c\
      env.c\
      ft_env.c

OBJ= $(SRCS:%.c=%.o)
OBJDIR= obj

LIB= libft.a
LIBDIR= libft/

NAME=minishell

all: $(LIB) $(NAME)

$(NAME): $(addprefix $(OBJDIR)/, $(OBJ)) $(LIBDIR)$(LIB)
	$(CC) $(CFLAGS) -o $(NAME) $(addprefix $(OBJDIR)/, $(OBJ)) -L$(LIBDIR) -lft

$(OBJDIR)/%.o: %.c $(HEADERS) | $(OBJDIR)
	$(COMPILE.c) $< -o $@

$(OBJDIR):
	-mkdir -p $@

$(LIBDIR)$(LIB):
	$(MAKE) -C $(LIBDIR)

$(LIB):
	$(MAKE) -C $(LIBDIR)

main: $(LIB)
	$(CC) $(CFLAGS) -o test $(main) -L$(LIBDIR) -lft

clean:
ifeq ($(OPT), LIB)
	$(MAKE) clean -C $(LIBDIR)
else

endif
	-rm -rf $(OBJDIR) 
	@-rm -rf a.out test test.dSYM

fclean: clean
ifeq ($(OPT), LIB)
	$(MAKE) fclean -C $(LIBDIR)
else

endif		
	-rm -f $(NAME)
	@-rm -rf $(NAME).dSYM

re : fclean all
