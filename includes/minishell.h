/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/18 16:40:08 by lportay           #+#    #+#             */
/*   Updated: 2017/09/28 15:53:36 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <sys/ioctl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <signal.h>
# include <setjmp.h>
# include "libft.h"
# ifdef __linux__
#  include <sys/wait.h>
# endif

typedef struct	s_env
{
	char		*s;
	char		**input;
	char		**env;
	char		**path;
	t_hash		*local[HASHSIZE];

}				t_env;

extern pid_t g_child_pid;

void			minishell(char **env);
void			ft_printenv(const char *name, char **env);
void			wrap_exit(int status, t_env *e);
int				ft_env(char **input, char **env, char **path);
int				sh_setenv(char **input, char ***env, t_env *e);
int				sh_unsetenv(char **input, char ***env, t_env *e);
int				error(char *str);
void			fatal_error(char *str, t_env *e);
int				tabsize(char **tab);
int				redirect(char **input, t_env *e);
int				search_path(char **path, char **input, char **env);
void			siginthandler(int signum);
void			wrap_expand(char **str, char *expr);
void			no_tabs(char *str);

#endif
