/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/24 22:11:41 by lportay           #+#    #+#             */
/*   Updated: 2017/09/28 16:16:58 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_printenv(const char *name, char **env)
{
	char *value;

	if (!name)
		while (*env)
			ft_printf("%s\n", *env++);
	else
	{
		if ((value = ft_getenv(name, env)) != NULL)
			ft_printf("%s\n", value);
	}
}

int		sh_setenv(char **input, char ***env, t_env *e)
{
	int i;

	i = tabsize(input);
	if (i >= 4)
		return (error("Too many arguments.\n"));
	if ((i == 3 || i == 2) && *input[1] >= '0' && *input[1] <= '9')
		return (error("Variable name must begin with a letter.\n"));
	if (i == 3)
		ft_setenv(input[1], input[2], 1, env);
	else if (i == 2)
		ft_setenv(input[1], "", 1, env);
	else if (i == 1)
		ft_printenv(NULL, *env);
	del_array((void **)e->path);
	free(e->path);
	e->path = ft_strsplit(ft_getenv("PATH", e->env), ':');
	return (0);
}

int		sh_unsetenv(char **input, char ***env, t_env *e)
{
	int i;

	i = tabsize(input);
	if (i == 1)
		return (error("Too few arguments.\n"));
	else
	{
		while (*input != NULL)
			ft_unsetenv(*input++, env);
	}
	del_array((void **)e->path);
	free(e->path);
	e->path = ft_strsplit(ft_getenv("PATH", e->env), ':');
	return (0);
}
