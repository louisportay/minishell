/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/27 13:44:40 by lportay           #+#    #+#             */
/*   Updated: 2017/09/28 15:48:34 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int	wrap_exec(char *fpath, char **input, char **env)
{
	pid_t child_pid;
	pid_t stat_loc;

	child_pid = fork();
	if (child_pid == 0)
	{
		*input = "";
		execve(fpath, input, env);
		exit(EXIT_SUCCESS);
	}
	else if (child_pid < 0)
	{
		error("Failed to fork the process\n");
		exit(EXIT_FAILURE);
	}
	else
	{
		g_child_pid = child_pid;
		waitpid(child_pid, &stat_loc, WUNTRACED);
		g_child_pid = 0;
	}
	ft_strdel(&fpath);
	return (0);
}

int	simple_exec(char **input, char **env)
{
	pid_t	child_pid;
	pid_t	stat_loc;
	char	*tmp;

	child_pid = fork();
	if (child_pid == 0)
	{
		tmp = ft_strdup(*input);
		*input = "";
		execve(tmp, input, env);
		ft_strdel(&tmp);
		exit(EXIT_SUCCESS);
	}
	else if (child_pid < 0)
	{
		error("Failed to fork the process\n");
		exit(EXIT_FAILURE);
	}
	else
	{
		g_child_pid = child_pid;
		waitpid(child_pid, &stat_loc, WUNTRACED);
		g_child_pid = 0;
	}
	return (0);
}

int	search_path(char **path, char **input, char **env)
{
	char	*fpath;
	char	*tmp;

	if (access(*input, X_OK) == 0)
		return (simple_exec(input, env));
	if (path == NULL)
		return (error("Empty Path, command not found\n"));
	if ((tmp = ft_strjoin("/", *input)) == NULL)
		return (error("Not enough Memory left\n"));
	fpath = ft_strjoin(*path, tmp);
	while ((*path) != NULL)
	{
		if (!fpath)
			return (error("Not enough Memory left\n"));
		if (access(fpath, X_OK) == 0)
		{
			ft_strdel(&tmp);
			return (wrap_exec(fpath, input, env));
		}
		ft_strdel(&fpath);
		fpath = ft_strjoin(*(++path), tmp);
	}
	ft_strdel(&fpath);
	ft_strdel(&tmp);
	return (error("command not found\n"));
}
