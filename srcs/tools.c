/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/10 19:15:02 by lportay           #+#    #+#             */
/*   Updated: 2017/09/21 11:29:01 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		error(char *str)
{
	if (str)
		ft_putstr(str);
	return (-1);
}

void	fatal_error(char *str, t_env *e)
{
	if (e->env)
	{
		del_array((void **)e->env);
		free(e->env);
	}
	hashdel(e->local, "PS1", ft_memdel);
	if (e->path)
	{
		del_array((void **)e->path);
		free(e->path);
	}
	if (e->s != NULL)
		ft_strdel(&e->s);
	if (str)
		ft_putstr(str);
	exit(EXIT_FAILURE);
}

int		tabsize(char **tab)
{
	int i;

	i = 0;
	if (tab == NULL)
		return (-1);
	while (*tab++)
		i++;
	return (i);
}

void	wrap_expand(char **str, char *expr)
{
	char *tmp;

	tmp = *str;
	*str = ft_strexpand(*str, '~', expr);
	if (tmp != *str)
		ft_strdel(&tmp);
}

void	no_tabs(char *str)
{
	while (*str)
	{
		if (ft_isspace(*str))
			*str = ' ';
		str++;
	}
}
