/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/18 18:28:41 by lportay           #+#    #+#             */
/*   Updated: 2017/09/21 21:18:51 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void		wrap_exit(int status, t_env *e)
{
	ft_putstr("exit\n");
	del_array((void **)e->env);
	free(e->env);
	if (e->s != NULL)
	{
		del_array((void **)e->input);
		free(e->input);
	}
	ft_strdel(&e->s);
	del_array((void **)e->path);
	free(e->path);
	hashdel(e->local, "PS1", ft_memdel);
	exit(status);
}

static char	*testdir(char *dirpath)
{
	struct stat buf;

	if (stat(dirpath, &buf) == -1)
		return ("No such file or directory\n");
	if (S_ISDIR(buf.st_mode) == 0)
		return ("Not a directory\n");
	if ((S_IXUSR & buf.st_mode) != S_IXUSR)
		return ("Permission Denied\n");
	return (NULL);
}

int			cd(char **input, char ***env)
{
	char	buf[512];
	char	*err;
	char	*tmp;

	tmp = NULL;
	if (input[0] && input[1])
		return (error("too many arguments\n"));
	if (!(*input))
		tmp = ft_strdup(ft_getenv("HOME", *env));
	else if (ft_strcmp(*input, "-") == 0)
		tmp = ft_strdup(ft_getenv("OLDPWD", *env));
	if ((err = testdir(tmp == NULL ? *input : tmp)) != NULL)
	{
		ft_strdel(&tmp);
		return (error(err));
	}
	chdir(tmp == NULL ? *input : tmp);
	ft_strdel(&tmp);
	if ((getcwd(buf, 512)) == NULL)
		return (error("error retrieving current directory\n"));
	ft_setenv("OLDPWD", ft_getenv("PWD", *env), 1, env);
	ft_setenv("PWD", buf, 1, env);
	return (0);
}

void		echo(char **input)
{
	bool nl;

	nl = true;
	if (*(++input) == NULL)
		return (ft_putchar('\n'));
	if (ft_strcmp(*input, "-n") == 0)
	{
		nl = false;
		input++;
	}
	while (*input != NULL)
	{
		ft_putstr(*input++);
		if (*input != NULL)
			ft_putchar(' ');
	}
	if (nl == true)
		ft_putchar('\n');
}

int			redirect(char **input, t_env *e)
{
	if (ft_strncmp(input[0], "exit", 4) == 0)
	{
		if (input[1] != NULL && input[2] == NULL)
			wrap_exit(ft_atoi(input[1]), e);
		else if (input[1] != NULL && input[2] != NULL)
			return (error("too many arguments\n"));
		wrap_exit(EXIT_SUCCESS, e);
	}
	else if (ft_strncmp(input[0], "cd", 2) == 0)
		cd(++input, &e->env);
	else if (ft_strncmp(input[0], "echo", 4) == 0)
		echo(input);
	else if (ft_strcmp(input[0], "env") == 0)
		ft_env(input, e->env, e->path);
	else if (ft_strncmp(input[0], "setenv", 6) == 0)
		sh_setenv(input, &e->env, e);
	else if (ft_strcmp(input[0], "unsetenv") == 0)
		sh_unsetenv(input, &e->env, e);
	else if (ft_strncmp(input[0], "printenv", 8) == 0)
		ft_printenv(input[1], e->env);
	else
		return (1);
	return (0);
}
