/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/05 15:42:37 by lportay           #+#    #+#             */
/*   Updated: 2017/09/28 16:10:33 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

pid_t g_child_pid = 0;

void		siginthandler(int signo)
{
	(void)signo;
	if (g_child_pid != 0)
	{
		kill(g_child_pid, SIGTERM);
		ft_putchar('\n');
		return ;
	}
	ft_putstr("\nminishell$ ");
}

int			init_variables(t_env *e, char **env)
{
	t_hash	*h;
	int		i;
	char	*tmp;

	i = 0;
	e->s = NULL;
	e->input = NULL;
	if ((e->env = ft_copyenv(env)) == NULL)
		return (-1);
	if ((tmp = ft_getenv("SHLVL", e->env)) != NULL)
	{
		ft_setenv("SHLVL", tmp = ft_itoa(ft_atoi(tmp) + 1), 1, &e->env);
		ft_strdel(&tmp);
	}
	e->path = ft_strsplit(ft_getenv("PATH", e->env), ':');
	while (i < HASHSIZE)
		e->local[i++] = NULL;
	if ((h = hashcreate("PS1", "minishell$ ", 12)) == NULL)
		return (-1);
	if (hashinsert(e->local, h, 0, ft_memdel) == -1)
		return (-1);
	return (0);
}

void		minishell(char **env)
{
	t_env e;

	if (init_variables(&e, env) == -1)
		fatal_error("Can't initialize shell variables\n", &e);
	signal(SIGINT, siginthandler);
	while (1)
	{
		ft_putstr((char *)hashlookup(e.local, "PS1")->data);
		if (get_next_line(0, &e.s) == -1)
			fatal_error("Can't read STDIN\n", &e);
		if (e.s == NULL)
			wrap_exit(EXIT_SUCCESS, &e);
		no_tabs(e.s);
		wrap_expand(&e.s, ft_getenv("HOME", e.env));
		if ((e.input = ft_strsplitquote(e.s, ' ')) == NULL &&
				ft_stroccur(e.s, ' ') != ft_strlen(e.s))
			error("Can't parse the input\n");
		if (e.input && redirect(e.input, &e) == 1)
			search_path(e.path, e.input, e.env);
		ft_strdel(&e.s);
		del_array((void **)e.input);
		ft_strdel((char **)&e.input);
	}
}
