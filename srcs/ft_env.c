/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/26 11:59:18 by lportay           #+#    #+#             */
/*   Updated: 2017/09/21 21:19:35 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	clean_exit(int status, char **tmpenv)
{
	del_array((void **)tmpenv);
	free(tmpenv);
	return (status);
}

static int	unset(char **input, char ***tmpenv)
{
	if (*input == NULL)
	{
		return (clean_exit(error(\
"option requires an argument -- u\nusage: env [-i] [-u name] [name=value ...]\
[utility [argument ...]]\n"), *tmpenv));
	}
	ft_unsetenv(*input, tmpenv);
	return (0);
}

static void	set(char *tmp, char *input, char ***tmpenv)
{
	*tmp = '\0';
	ft_setenv(input, tmp + 1, 1, tmpenv);
}

int			ft_env(char **input, char **env, char **path)
{
	char	**tmpenv;
	char	*tmp;

	if ((tmpenv = ft_copyenv(env)) == NULL)
		return (error("Empty environment\n"));
	while (*++input != NULL)
	{
		if (ft_strncmp(*input, "-i", 2) == 0)
			del_array((void **)tmpenv);
		else if (ft_strncmp(*input, "-u", 2) == 0)
		{
			if (unset(++input, &tmpenv) == -1)
				return (-1);
		}
		else if ((tmp = ft_strchr(*input, '=')) != NULL)
			set(tmp, *input, &tmpenv);
		else
			return (clean_exit(search_path(path, input, tmpenv), tmpenv));
	}
	ft_printenv(NULL, tmpenv);
	return (clean_exit(0, tmpenv));
}
