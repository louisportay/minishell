/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lportay <lportay@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/16 19:21:40 by lportay           #+#    #+#             */
/*   Updated: 2017/08/24 20:59:27 by lportay          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <stdlib.h>
# include <unistd.h>
# define BUFF_SIZE 512
# include "ft_stdio.h"
# include "ft_stdlib.h"
# include "ft_string.h"
# include "ft_ctype.h"
# include "ft_wchar.h"
# include "buffer.h"
# include "ft_dlst.h"
# include "ft_lst.h"
# include "other.h"
# include "ft_hash.h"
# include "ft_btree.h"

#endif
